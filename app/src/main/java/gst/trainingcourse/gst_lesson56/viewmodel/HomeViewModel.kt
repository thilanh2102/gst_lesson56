package gst.trainingcourse.gst_lesson56.viewmodel

import android.content.Context
import androidx.databinding.BaseObservable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import gst.trainingcourse.gst_lesson56.model.ItemData
import gst.trainingcourse.gst_lesson56.view.MainActivity

class HomeViewModel: BaseObservable(), OnCLickMenu {
    private var adapterItem= Adapter(ItemData.list, this)
    private val _adapter = MutableLiveData<Adapter>()
    val adapter: LiveData<Adapter>
        get() = _adapter
    fun onClick(i:  Int){
        _adapter.value= adapterItem
    }

    override fun onClickMenuItem(position: Int) {
        TODO("Not yet implemented")
    }
}