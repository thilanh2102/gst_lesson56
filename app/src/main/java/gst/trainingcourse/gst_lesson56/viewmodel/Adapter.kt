package gst.trainingcourse.gst_lesson56.viewmodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import gst.trainingcourse.gst_lesson56.R
import gst.trainingcourse.gst_lesson56.model.Item

class Adapter(private val list : ArrayList<Item>, private val onCLickMenu: OnCLickMenu): RecyclerView.Adapter<Adapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view= LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
        return Holder(view)
    }

    class Holder(item : View) : RecyclerView.ViewHolder(item) {
        val tvUserName: TextView=item.findViewById(R.id.tv)
        val icon: ImageView=item.findViewById(R.id.img)
        val item: CardView = item.findViewById(R.id.itemMenu)
    }


    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item= list[position]
        holder.tvUserName.text = item.name
        holder.icon.setImageResource(item.icon)
        holder.item.setOnClickListener(View.OnClickListener {
            onCLickMenu.onClickMenuItem(position)
        })
    }

    override fun getItemCount(): Int {
        return list.size
    }
}