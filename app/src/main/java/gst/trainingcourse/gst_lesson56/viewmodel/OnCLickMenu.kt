package gst.trainingcourse.gst_lesson56.viewmodel

interface OnCLickMenu {
    fun onClickMenuItem(position: Int)
}