package gst.trainingcourse.gst_lesson56.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import gst.trainingcourse.gst_lesson56.R
import gst.trainingcourse.gst_lesson56.databinding.ActivityMainBinding
import gst.trainingcourse.gst_lesson56.model.Item
import gst.trainingcourse.gst_lesson56.viewmodel.Adapter
import gst.trainingcourse.gst_lesson56.viewmodel.AdapterMenu
import gst.trainingcourse.gst_lesson56.viewmodel.HomeViewModel
import gst.trainingcourse.gst_lesson56.viewmodel.OnCLickMenu

class MainActivity : AppCompatActivity(), OnCLickMenu {
    private lateinit var binding: ActivityMainBinding
    private val homeViewModel= HomeViewModel()
    private lateinit var adapter1: AdapterMenu


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.homeViewModel=homeViewModel

        homeViewModel.adapter.observe(this,  { adapter ->
            if (adapter!=null) {
                init(adapter)
            }
        })

        val myAdapter = Adapter(menu(),this)
        binding.recyc2.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.HORIZONTAL,false)
        }
    }
    private fun init(adapter: Adapter){

    }


    private fun menu(): ArrayList<Item>{
        var list = ArrayList<Item>()
        val s1 = Item(R.drawable.ba,"Draw")
        val s2 = Item(R.drawable.hai,"Layout")
        val s3 = Item(R.drawable.icon_circle,"")
        val s4 = Item(R.drawable.bon,"Text")
        val s5 = Item(R.drawable.nam,"Custom")
        list.add(s1)
        list.add(s2)
        list.add(s3)
        list.add(s4)
        list.add(s5)
        return list
    }
    private fun menu21(): ArrayList<Item>{
        var list = ArrayList<Item>()
        val s1 = Item(R.drawable.mot1,"Draw1")
        list.add(s1)
        return list
    }
    private fun menu22(): ArrayList<Item>{
        var list = ArrayList<Item>()
        val s1 = Item(R.drawable.hai1,"Layout1")
        val s2 = Item(R.drawable.hai2,"Layout1")
        list.add(s1)
        list.add(s2)
        return list
    }
    private fun menu23(): ArrayList<Item>{
        var list = ArrayList<Item>()
        val s1 = Item(R.drawable.ba1,"Three1")
        val s2 = Item(R.drawable.ba2,"Three2")
        val s3 = Item(R.drawable.ba3,"Three3")
        list.add(s1)
        list.add(s2)
        list.add(s3)
        return list
    }
    private fun menu24(): ArrayList<Item>{
        var list = ArrayList<Item>()
        val s1 = Item(R.drawable.bon1,"Text1")
        val s2 = Item(R.drawable.bon2,"Text2")
        val s3 = Item(R.drawable.bon3,"Text3")
        val s4 = Item(R.drawable.bon4,"Text4")
        list.add(s1)
        list.add(s2)
        list.add(s3)
        list.add(s4)
        return list
    }
    private fun menu25(): ArrayList<Item>{
        var list = ArrayList<Item>()
        val s1 = Item(R.drawable.nam1,"Custom1")
        val s2 = Item(R.drawable.nam2,"Custom2")
        val s3 = Item(R.drawable.nam3,"Custom3")
        val s4 = Item(R.drawable.nam4,"Custom4")
        val s5 = Item(R.drawable.nam5,"Custom5")
        val s6 = Item(R.drawable.nam6,"Custom6")
        val s7 = Item(R.drawable.nam7,"Custom7")
        val s8 = Item(R.drawable.nam8,"Custom8")
        val s9 = Item(R.drawable.nam9,"Custom9")
        val s10 = Item(R.drawable.nam10,"Custom10")
        list.add(s1)
        list.add(s2)
        list.add(s3)
        list.add(s4)
        list.add(s5)
        list.add(s6)
        list.add(s7)
        list.add(s8)
        list.add(s9)
        list.add(s10)
        return list
    }


    override fun onClickMenuItem(position: Int) {
        var listMenu2 = ArrayList<Item>()
        when(position){
            0 -> listMenu2=menu21()
            1 -> listMenu2=menu22()
            2 -> listMenu2=menu23()
            3 -> listMenu2=menu24()
            4 -> listMenu2=menu25()
        }
        adapter1= AdapterMenu(listMenu2)
        binding.recyc.apply {
            layoutManager = ArcLayoutManager(this@MainActivity, 0)
            adapter= adapter1
        }
    }
}