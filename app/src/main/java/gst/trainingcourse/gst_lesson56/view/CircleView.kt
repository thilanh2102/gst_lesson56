package gst.trainingcourse.gst_lesson56.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import gst.trainingcourse.gst_lesson56.R

class CircleView: View {
    private var  paint=Paint()
    private var  paintRec=Paint()
    private var color=Color.parseColor("#4284f5")
    constructor(context: Context?): super(context)
    constructor(context: Context?, attrs: AttributeSet?): super(context, attrs){
        initView(context, attrs)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr){
        initView(context, attrs)
    }

init {
    paint.color= Color.parseColor("#4284f5")
    paint.isAntiAlias=true//
    paint.strokeWidth=5f//
    paint.style=Paint.Style.FILL_AND_STROKE// nét vẽ thể hiện như nào

    paintRec.color= Color.parseColor("#f54281")
    paintRec.isAntiAlias=true//
    paintRec.strokeWidth=5f//
    paintRec.style=Paint.Style.FILL_AND_STROKE// nét vẽ thể hiện như nào
}

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec)
    }


    private fun initView(context: Context?, attrs: AttributeSet?){
        val typedArray= context?.obtainStyledAttributes(attrs, R.styleable.CircleView)
        color=typedArray?.getColor(R.styleable.CircleView_cv_color, color)?: color
        typedArray?.recycle()


    }
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val wid=context.resources.displayMetrics.widthPixels
        val hei=context.resources.displayMetrics.heightPixels
        val left= -wid*1/3f
        val top= hei-wid*3/4f
        val right= wid*4/3f
        val bottom= wid+hei.toFloat()
        paint.color=color
        paintRec.color=Color.parseColor("#ffffff")

        val recF= RectF(left,top,right,bottom)
        canvas?.drawOval(recF, paint)
        val recF2= RectF(left+200,top+200,right-200,bottom-300)
        canvas?.drawOval(recF2, paintRec)
    }
}